# Add any require_relative statements here
require_relative 'contact'

class CRM

  @@all_contacts = Contact.all

  def self.run(name)
    @@crm = CRM.new(name)
    @@crm.main_menu
  end

  def initialize(name)
    @name = name
  end

  def main_menu
  while true
    print_main_menu
    user_selected = gets.to_i
    call_option(user_selected)
  end
end

def print_main_menu
  puts '[1] Add a new contact'
  puts '[2] Modify an existing contact'
  puts '[3] Delete a contact'
  puts '[4] Display all the contacts'
  puts '[5] Search by attribute'
  puts '[6] Exit'
  puts 'Enter a number: '
end

  def call_option(user_selected)
    case user_selected
    when 1 then add_new_contact
    when 2 then modify_existing_contact
    when 3 then delete_contact
    when 4 then display_all_contacts
    when 5 then search_by_attribute
    when 6 then exit
    end
  end


  def add_new_contact
  print 'Enter First Name: '
  first_name = gets.chomp

  print 'Enter Last Name: '
  last_name = gets.chomp

  print 'Enter Email Address: '
  email = gets.chomp

  print 'Enter a Note: '
  note = gets.chomp

  new_contact = Contact.create(first_name, last_name, email, note)
end

  def modify_existing_contact
    puts 'Enter id number of contact you\'d like to modify: '
    search_id = gets.strip
    puts "The record you\'ve chosen to modify is #{Contact.find(search_id)} Is this correct? Type yes or no:"
    answer = gets.chomp.downcase
    if answer == "yes" then
      puts '[1] To modify contact first name'
      puts '[2] To modify contact last name'
      puts '[3] To modify contact email'
      puts '[4] To modify contact note'
      puts '[5] To return to the main menu'
      attribute_chosen = gets.to_i
          case attribute_chosen
            when 1 then attribute = first_name
            when 2 then attribute = last_name
            when 3 then attribute = email
            when 4 then attribute = note
            when 5 then @@crm.main_menu
          end
      puts 'Enter new value for contact:'
       new_value = gets.chomp
      Contact.update(attribute, new_value)
    else
      @@crm.main_menu
        end
  end

  def delete_contact
    self.display_all_contacts
      puts 'Enter id number of contact you\'d like to delete: '
      search_id = gets.strip
      puts "The record you\'ve chosen to modify is #{Contact.find(search_id)} Is this correct? Type yes or no:"
      delete_confirmation = gets.chomp.downcase
      if delete_confirmation == 'yes' then
        Contact.delete
        @@crm.main_menu
      else
        @@crm.main_menu
      end
  end

  def display_all_contacts
    self.display_contacts @@all_contacts
  end

  def search_by_attribute
    puts "Which field do you want to search?"
    puts "[1] First name"
    puts "[2] Last name"
    puts "[3] Email &#64;ddress"
    puts "[4] Contact note"
    field = gets.chomp
    print  "Which entry are you looking for?: "
    value = gets.chomp
      case
      when field == 1 then result = @@all_contacts.select {|contact| contact.first_name == value}
      when field == 2 then result = @@all_contacts.select {|contact| contact.last_name == value}
      when field == 3 then result = @@all_contacts.select {|contact| contact.email == value}
      when field == 4 then result = @@all_contacts.select {|contact| contact.note.include?"value"}
      end
    self.display_contacts result
  end

  # This method should accept as its argument an array of contacts
  # and display each contact in the contacts array
  def display_contacts(contacts)
    contacts.each do |contact|
      puts ""
      puts "ID = #{contact.id}"
      puts "******".center(20)
      puts "Name: #{contact.full_name}"
      puts "Email: #{contact.email}"
      puts "Note: #{contact.note}"
      puts "******".center(20)
    end
  end


  # Add other methods here, if you need them.

end

# Run the program here (See 'Using a class method`)

CRM.run 'Bitmaker CRM'
