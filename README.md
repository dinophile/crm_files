# Contact Management app without Rails

##Introduction/Purpose

This is pretty cool to me. We built a very basic contact manager just prior to learning anything about Rails. 
This is built entirely with Ruby, HTML, and CSS. 
Just a few days after completing this assignment my mind was blown when we typed rails new and hit enter!

I'm glad we built this from scratch to illustrate just why we use frameworks when building apps that in the real world
will likely need to be scalable. There's no way you could build the next Facebook like this and have the same
functionality users have come to expect of contemporary webapps!

##Deployment

This is a simple web project, deployment can be on any web server or local file system.
