class Contact

attr_accessor :first_name, :last_name, :email, :note
attr_reader :id

@@contacts = []
@@id = 1


  def initialize(id, first_name, last_name, email, note)
    @id = id
    @first_name = first_name
    @last_name = last_name
    @email = email
    @note = note
  end

  def full_name
    "#{first_name} #{last_name}"
    # => or [first_name, last_name].join(" ") but the first line is nicer
  end

  def self.create(first_name, last_name, email, note)
    # => 1. create an new contact
    new_contact = new(@@id, first_name, last_name, email, note)
    # => 2. add it to our list of contacts (@@contacts array)
    @@contacts << new_contact
    # => 3. increment the next id
    @@id += 1
    # => 4. return the newly created contact
    new_contact
  end

  def self.all
    @@contacts
  end

   # This method should take an integer id argument
  # and return the contact that corresponds to the id
  def self.find(id)
    @@contacts.find {|contact| contact.id == id }
  end

  # This method should take two string arguments (attribute, value)
  # It should look for the contact with the attribute corresponding to the value
  # and return that contact
  #
  # For example:find
  #   Contact.find_by('first_name', 'Joe')
  # should return the first contact with the first_name of Joe.
  def self.find_by(attribute, value)
    @@contacts.select{ |contact| contact.send(attribute) == value }
  end


  # This method takes no arguments
  # It should delete all of the contacts from memory
  def self.delete_all
    @@contacts.clear
  end


  # This method should take two string arguments (attribute, value)
  # It should update the current instance of contact with the new value of the attribute
  #
  # For example:
  #   mary.update('email', 'mary_new_email@gmail.com')
  # should update mary's email address to the new one.
  def update(attribute, value)
    self.send(attribute+"=", value)
  end

  # This method takes no arguments
  # It should delete the current instance of contact from memory
  def delete
    @@contacts.delete(self)
  end

end

Contact.create("A","B", "me@me.com", "Woo")
Contact.create("C","D", "him@him.com", "Yay")
